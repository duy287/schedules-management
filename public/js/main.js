function readURL(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function readURL1(input) {
    if (input.files && input.files[0]) {
        let reader = new FileReader();
        reader.onload = function(e) {
            $('#imagePreview1').css('background-image', 'url('+e.target.result +')');
            $('#imagePreview1').hide();
            $('#imagePreview1').fadeIn(650);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function() {
    readURL(this);
});
$("#imageFavicon").change(function() {
    readURL1(this);
});
$( document ).ready(function() {
    $("input , textarea").each(function() {
        let element = $(this);
        if (element.val() !== "") {
            $(this).closest('.form-group').addClass('label-input');
        }
    });
    if ($('.form-group input ,.form-group  textarea').val()) {
        $(this).closest('.form-group').addClass('label-input');
    }
    $('input,textarea').focus(function(event) {
        $(this).closest('.form-group').addClass('label-input');
    });
    $('input, textarea').blur(function() {
        if (!$(this).val()) {
            $(this).closest('.form-group').removeClass('label-input');
        }
    });

});
