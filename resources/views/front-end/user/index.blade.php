@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <a href="{{ route('user.add') }}" class="btn btn-primary float-right"><i class="fa fa-plus"></i> {{ __('action.add') }}</a>
          </div>
          <div class="card-body">
            <table id="list-item" class="table table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th>{{ __('user.name') }}</th>
                  <th>{{ __('user.username') }}</th>
                  <th>{{ __('user.phone') }}</th>
                  <th>{{ __('user.email') }}</th>
                  <th>{{ __('user.created_at') }}</th>
                  <th>{{ __('action.action') }}</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- confirm dialog-->
    <div class="modal fade" id="confirmDeleteItem" tabindex="-1" role="dialog" aria-labelledby="modelLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header bg-warning">
                  <strong>{{__('action.delete_confirm')}}</strong>
              </div>
              <div id="confirmMessage" class="modal-body">
                  {{__('action.delete_sure')}}
              </div>
              <div class="modal-footer">
                  <button type="button" id="confirmCancel" class="btn btn-default btn-cancel" data-dismiss="modal">
                  {{__('action.cancel')}}
                  </button>
                  <button type="button" id="btnConfirmDelete" class="btn btn-danger btn-ok" onclick="deleteItem($(this).val())">
                  {{__('action.delete')}}
                  </button>
              </div>
          </div>
      </div>
  </div>
    <!-- /.row -->
</div>
@endsection

@push('script')
 <!-- DataTables -->
<script>
    @if(session('language')=='ja')
        url = "{{asset('lib/datatables/table.json')}}";
    @endif
    $.fn.dataTable.ext.errMode = 'throw'; 
    $("#list-item").DataTable({
        language: {
            url: url
        },
        processing: true,
        pageLength: 20,
        serverSide: true,
        ajax: {
            "url": "{{ route('user.api') }}",
            "type": 'GET',
        },
        columns: [
            { "data": "name" },
            { "data": "username"},
            { "data": "phone" },
            { "data": "email" },
            {
                "data": "created_at",
                "render": function (data) {
                    let date = new Date(data);
                    let month = date.getMonth() + 1;
                    return date.getFullYear() + "-" + (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + "-" + (month < 10 ? "0" + month : month);
                }
            },
        ],
        columnDefs : [{
            "targets": 5,
            "data": null,
            "render": function (data) {
                edit_href = "{{ url('users/edit') }}/" + data.id;
                delete_href = ' onclick = checkItemExisted(' + data.id + ') ';
                return '<button class="btn btn-sm btn-danger" ' + delete_href + ' data-toggle="modal" data-target="#confirmDeleteItem" >' +
                    '<span class="fa fa-trash" ></span></button> ' +
                    '<a class="btn btn-success btn-sm" href=' + edit_href + '><span class="fa fa-edit"></span></a>';
            }
        }],
    });

    function checkItemExisted(id) {
        $('#btnConfirmDelete').val(id);
    }

    function deleteItem(id) {
        request = $.ajax({
            url: "{{ route('user.delete') }}",
            method: "POST",
            data: {
                "id" : id
            }
        }).done(function (data) {
            if(data.status == 403){
                $("#confirmDeleteItem").modal('hide');
                alert(data.message);
            }
            else if (data.status == 1) {
                location.reload(true);
            }
            else {
                $("#confirmDeleteItem").modal('hide');
                alert('You can not delete yourself !!');
            }
        })
    }
</script>
@endpush
