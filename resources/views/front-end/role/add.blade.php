@extends('layouts.app')

@section('content')
<div class="container-full">
    <div class="content">
        <div class="container-fluid">
            <form role="form" method="POST" action="{{ route('role.post_add') }}" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-7">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">{{__('role.create_role')}}</h4>
                            <p class="card-category">{{__('role.create_role_title')}}</p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="form-group bmd-form-group @error('name') has-danger @enderror ">
                                            <label class="bmd-label-floating">{{__('role.role_name')}}</label>
                                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                            @error('name')<span class="text-danger">{{ $message }}</span>@enderror
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-group bmd-form-group">
                                            <label class="bmd-label-floating">{{__('role.description')}}</label>
                                            <textarea class="form-control" rows="2" name="description">{{ old('description') }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h6 class="card-title">{{__('role.permissions')}}</h6>
                        </div>
                        <div class="card-body">
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" id="check-all">
                                    <div class="check-all">
                                        Check all
                                    </div>
                                    <span class="form-check-sign"><span class="check"></span></span>
                                </label>
                            </div>
                            <?php $lang = session('language') ? session('language'): 'en' ;?>

                            @foreach($permissions as $key => $value)
                            <div class="form-check check-roles">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" name="permissions[]" value="{{ $key }}"
                                        {{ isset(old('permissions')[$key]) && old('permissions')[$key] ? 'checked' : '' }}>
                                    {{ $value[$lang] }}
                                    <span class="form-check-sign"><span class="check"></span></span>
                                </label>
                            </div>
                            @endforeach
                            <hr>
                            <button type="submit" class="btn btn-primary pull-right">{{__('action.save')}}</button>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        $("#check-all").click(function(){
            $('.check-roles input:checkbox').not(this).prop('checked', this.checked);
        });
    </script>

@endpush
