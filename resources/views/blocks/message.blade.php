@if (Session::has('flash_message'))
<div class="col-md-4" id="custom-alert">
    <div class="alert alert-{{ Session::get('flash_level') }} alert-dismissible fade show"  role="alert">
        <strong>Message:</strong> {{ Session::get('flash_message') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>
<script>
    setTimeout(function(){ 
        $("#custom-alert").slideUp();         
    }, 3000); 
</script>
@endif