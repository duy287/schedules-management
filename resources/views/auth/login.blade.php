<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ config('app.name', 'SKR') }} | Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#007bff"/>
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#007bff">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#007bff">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('lib/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{asset('lib/font-awesome/css/fontawesome.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('lib/adminlte/css/adminlte.min.css')}}">

    <!-- Google Font: Source Sans Pro -->
    <style>
        body {
            width: 360px;
            height: 464px;
            z-index: -9999;
            margin-left: auto;
            margin-right: auto;
            margin-top: 80px;
            margin-bottom: auto;
            overflow: hidden;
        }
        canvas {
            background-color: #e9ecef;
            display: block;
            position: fixed;
            top: 0;
            left: 0;
            z-index: -9999;

        }
    </style>
</head>

<body>
<!-- background animate -->
<canvas id="dot-connect"></canvas>
<!-- /.background animate -->

<!-- Login -->
<div class="login-box">
    <div class="login-logo ">
        <a href="#"><b class="text-primary font-weight-bold">CALC</b>-ADMIN</a>
    </div>
    <!-- /.login-logo -->
    <div class="card card-primary card-outline">
        <div class="card-body login-card-body">
            <p class="login-box-msg">{{__('auth.session')}}</p>
            <form method="POST" action="{{ route('login') }}">
            @csrf
            <!--Username-->
                <div class="row">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-user"></i></span>
                        </div>
                        <input id="email" type="text" class="form-control @error('username') is-invalid @enderror"
                                placeholder="{{__('auth.username')}}" name="username" value="{{ old('username') }}" autofocus>
                        @error('username')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <!--Password-->
                <div class="row">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fa fa-lock "></i></span>
                        </div>
                        <input id="password" type="password"
                                class="form-control @error('password') is-invalid @enderror" placeholder="{{__('auth.password')}} "
                                name="password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                        @enderror
                    </div>
                </div>

                <div class="row">
                    <button type="submit" style="margin: 15px 0 0px"
                            class="btn btn-primary btn-block btn-flat">{{__('auth.sign_in')}}</button>
                </div>
                <label class="form-check-label" for="remember" style="font-size: 14px">
                    <a href="{{url('/password/reset')}}">
                        {{__('auth.forgot')}}
                    </a>
                </label>

            </form>
        </div>
        <!-- /.login-card-body -->
    </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{asset('lib/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('lib/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- dot-connect -->
<script src="{{asset('lib/canvas-animation/dot-connect.min.js')}}"></script>
</body>

</html>
