{{--Start Navbar--}}
<nav class="navbar navbar-expand-lg  ">

    <div class="container-fluid ">
        <div class="navbar-wrapper">
            <a class="navbar-brand" href="javascript:;"></a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="javascript:;">
                        <i class="material-icons">dashboard</i>
                        <p class="d-lg-none d-md-block">
                            Stats
                        </p>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="http://example.com" id="notifications" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons">notifications</i>
                        <span class="notification">{{$total}}</span>
                        <p class="d-lg-none d-md-block">
                            Some Actions
                        </p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink"
                         id="list-notifications">
                            @if($total >0 )
                                @foreach ($list as $notification)
                                    <a class="dropdown-item" href="{{url('/detail-report/'.$notification->data['report_id'])}}">
                                        <img src="{{asset('storage/avatar/'.$notification->avatar)}}" class="avatar-notification">
                                        @if(empty($notification->data['to_id']))
                                            <span class="font-weight-bold">{{ $notification->name }}</span> {{__('layout.reminded')}}
                                        @else
                                            <span class="font-weight-bold">{{ $notification->name }}  {{__('layout.commented')}}</span>
                                        @endif
                                        <br>
                                        <span class="time-notification">
                                                {{Carbon\Carbon::parse( $notification->data['created_at'])->locale(app()->getLocale())->diffForHumans() }}
                                            </span>
                                    </a>
                                @endforeach
                            @else
                                <a class="dropdown-item" href="">{{__('layout.notification')}}  </a>
                            @endif
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link flex-row" href="javascript:;" id="languageDropdown" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons">language</i>
                        {{ App::getLocale() == 'en'  ? 'EN':' JP'  }}
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile"
                         id="list-language">
                        <a class="dropdown-item " href="{{url('lang/en')}}">English
                            <img src="{{asset('img/en.png')}}"></a>
                        <a class="dropdown-item" href="{{url('lang/ja')}}">Japan
                            <img src="{{asset('img/jp.png')}}"></a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

{{--<--End Nav-->--}}
