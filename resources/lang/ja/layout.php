<?php
return [
    //---- Dashboard
    'my_reports' => '私のレポート',
    'total' => '合計',
    'total_users'=> '総ユーザー',
    'total_comments' => 'コメント総数',
    'this_month' => '今月',
    'reports' => '報告書',
    'view_more' => 'もっと見る',
    'chat_box' => 'チャットボックス',
    'chat_box_title' => '他のユーザーとの新しいチャットを作成する',
    'reports_today' => '今日のレポート',
    'reports_today_title' => '今日のユーザーレポート',
    'table'=>[
        'name' => '名前',
        'title' => '題名',
        'time' => '時間'
    ],

    //---- Sidebar
    'user_profile' => 'ユーザープロフィール',
    'calendar_report' => 'カレンダーレポート',
    'list_user_report' => 'リストレポート',
    'users' => 'ユーザー',
    'roles' => '役割',
    'groups' => '団体',
    'activity_logs' => 'アクティビティログ',

    'dashboard'=>'ダッシュボード',
    'logout'=>'ログアウト',
    'notification'=>'空の通知',
    'reminded'=>'コメントであなたに言及しました',
    'commented'=>'あなたのコメント付きレポート',
    'setting'=>'設定',
];
