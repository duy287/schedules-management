<?php
return [
    //---- Roles
    'roles'            => '役割',
    'role_list'        => '役割リスト',
    'name'             => '名前',
    'role_name'        => '役割名',
    'select_role'      => 'ユーザー役割を選択',
    'description'      => '説明文',
    'permissions'      => '許可フラグ',
    'add_role'         => '役割を追加',
    'create_role'      => '役割を作成',
    'create_role_title'=> 'ユーザーアクセスを管理する役割を作成する',
    'edit_role'        => '役割を編集',
    'edit_role_title'  => 'ロールを編集してユーザーアクセスを変更',
    'created_by'       => 'によって作成された',
    'created_at'       => 'で作成',
    'select_user'      => 'ユーザーを選択'
];
