<?php
return [
    //---- Groups
    'groups'           => '団体',
    'group_list'       => 'グループリスト',
    'name'             => '名前',
    'group_name'       => 'グループ名',
    'description'      => '説明文',
    'add_group'        => 'グループを追加',
    'create_group'     => 'グループを作成',
    'create_new_a_group' => '新しいグループを作成する',
    'edit_group'       => 'グループを編集',
    'update_group_data' => 'グループデータを更新する',
    'created_at'       => 'で作成',
    'select_user'      => 'ユーザーを選択'
];
