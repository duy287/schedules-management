<?php
return [
    //---- Roles
    'roles'            => 'Roles',
    'role_list'        => 'Roles list',
    'name'             => 'Name',
    'role_name'        => 'Role name',
    'select_role'      => 'Select user role',
    'description'      => 'Description',
    'permissions'      => 'Permissions',
    'add_role'         => 'Add role',
    'create_role'      => 'Create role',
    'create_role_title'=> 'Create a role to manage user access',
    'edit_role'        => 'Edit role',
    'edit_role_title'  => 'Edit role to change user access',
    'created_by'       => 'Created by',
    'created_at'       => 'Created at',
    'select_user'      => 'Select User',
];
