<?php
return [
    //---- Groups
    'groups'           => 'Groups',
    'group_list'       => 'Group list',
    'name'             => 'Name',
    'group_name'       => 'Group name',
    'description'      => 'Description',
    'add_group'        => 'Add group',
    'create_group'     => 'Create group',
    'create_new_a_group' => 'Create new a group',
    'edit_group'       => 'Edit group',
    'update_group_data' => 'Update group data',
    'created_at'       => 'Created at',
    'select_user'      => 'Select User'
];
