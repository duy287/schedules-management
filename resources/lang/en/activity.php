<?php
return [
    //---- Activities
    'user' => 'User',
    'ip_address' => 'IP Address',
    'history' => 'History',
    'log_time' => 'Log time',
    'more_info' => 'More info',
    'from' => 'From',
    'to' => 'To',
    '404'=>'WE ARE SORRY, PAGE NOT FOUND!',
    '404_content' => 'THE PAGE YOU ARE LOOKING FOR MIGHT HAVE BEEN REMOVED HAD ITS NAME CHANGED OR IS TEMPORARILY UNAVAILABLE.',
    'go_home' => 'BACK TO HOME',
    'clear' => 'clear the logs',
    'clear_the_logs' => 'Are you sure clear the logs ?'
];
