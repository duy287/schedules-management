<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::create([
            'name' => 'admin',
            'permission' => json_encode(['manage-user', 'manage-role', 'manage-group']),
            'description' => "Has full permission",
            'created_by' => 'Developer'
        ]);
        
        User::create([
            'name' => 'admin',
            'username' => 'admin',
            'password' => bcrypt('admin123'),
            'email' => 'admin@gmail.com',
            'role_id' => $role->id
        ]);
    }
}
