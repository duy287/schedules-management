<?php
    /*
    |--------------------------------------------------------------------------
    | Permission user
    |--------------------------------------------------------------------------
    |
    */
    return[
        'comment'   => [
            'en' => 'Enable comment',
            'ja' => 'コメントを有効にする'
        ],
        'manage-user' => [
            'en' => 'Manage User',
            'ja' => 'ユーザーを管理'
        ],
        'manage-role' => [
            'en' => 'Manage Role',
            'ja' => '役割を管理'
        ],
        'manage-group' => [
            'en' => 'Manage Group',
            'ja' => 'グループを管理'
        ],
        'view-log' => [
            'en' => 'View Logs',
            'ja' => 'ログを表示'
        ],
        'setting' => [
            'en' => 'Setting System',
            'ja' => '設定システム'
        ]
    ];
