<?php

namespace App\Services;
use Illuminate\Http\Request;

interface UserService
{
    /*
    |--------------------------------------------------------------------------
    | Manage users, roles, groups in system
    |--------------------------------------------------------------------------
    |
    /**
     * Roles
     */
    function getRoles();
    function getRoleById($id);
    function addRole($request);
    function editRole($id, $request);
    function deleteRole($request);

    /**
     * users
     */
    function getUsers();
    function getUserById($id);
    function addUser($request);
    function editUser($id, $request);
    function deleteUser($request);
    // function getReportsFromUser($id);
    /**
     * User
     * @param $request
     */
    function ValidateUserEdit($request);
    function ValidateUserPass($request);
    function ChangeUser($request);
    function ChangePassword($request);

    /**
     * groups
    */
    function getGroups();
    function getGroupById($id);
    function addGroup($request);
    function editGroup($id, $request);
    function deleteGroup($request);
}
