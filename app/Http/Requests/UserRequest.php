<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('id');
        if($id){
            $rules = [
                'name' =>'required',
                'username' => 'required|min:4|unique:users,username,'.$id.',id',  //unique:table,column,except,idColumn
                'email' => 'required|email|unique:users,email,'.$id.',id',
                'avatar'=> 'image',
            ];
        }
        else{
            $rules = [
                'name' =>'required',
                'username'=>"required|min:4|unique:users,username",
                'email' => 'required|email|unique:users,email',
                'password' => 'required|min:6',
                'password_confirm' => 'required|same:password',
                'avatar'=> 'image',
            ];
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'name'=> trans('user.name'),
            'username'=> trans('user.username'),
            'email'=>trans('user.email'),
            'password'=>trans('user.password'),
            'password_confirm'=>trans('user.password_confirm'),
            'role'=>trans('user.role'),
            'avatar'=>trans('user.avatar'),
        ];
    }
}
