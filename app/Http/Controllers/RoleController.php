<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RoleRequest;
use App\Services\UserService;
use App\Role;

class RoleController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->middleware('auth');
        $this->userService = $userService;
    }

    public function getIndex(Request $request)
    {
        return view('front-end.role.index');
    }

    public function getAdd(Request $request)
    {
        $permissions = config('permission'); //get permission array
        return view('front-end.role.add',compact('permissions'));

    }

    public function postAdd(RoleRequest $request)
    {
        $this->userService->addRole($request);
        return redirect()->route('role.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.add_success')
        ]);
    }

    public function getEdit($id, Request $request)
    {
        $item = $this->userService->getRoleById($id);
        $permissions = config('permission'); //get permission array
        $role_permission = $item->getPermission();
        return view('front-end.role.edit',compact('item', 'permissions', 'role_permission'));
    }

    public function postEdit($id, RoleRequest $request)
    {
        $this->userService->editRole($id, $request);
        return redirect()->route('role.index')->with([
            'flash_level'   => 'success',
            'flash_message' => trans('action.update_success')
        ]);
    }

    public function postDelete(Request $request)
    {
        $role = Role::findOrFail($request->id);

        if ($role->users->count() > 0) {
            return response()->json([
                'status' => 0
            ]);
        }
        else {
            $this->userService->deleteRole($request);
            return response()->json([
                'status' => 1
            ]);
        }
    }
}
