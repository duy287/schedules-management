<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name', 'permission', 'description', 'created_by'
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }
    
    public function getPermission()
    {
        $permission = [];
        if ($this->permission) {
            $permission = (array) json_decode($this->permission);
        }
        return $permission;
    }
    
}
