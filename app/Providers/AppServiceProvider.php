<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\User;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.navbar',function ($view){
            $total =auth()->user()->unreadNotifications()->count();
            $list =auth()->user()->unreadNotifications->take(10);
            foreach ($list as $item){
                $user = User::findOrFail($item['data']['user_id'])->toArray();
                $item['name'] = $user['name'];
                $item['avatar'] = $user['avatar'];
            }
            $view->with(['total'=>$total , 'list'=>$list ]);
        });
    }
}
